import { Punto } from './app/punto';
import { Rectangulo } from './app/rectangulo';

$(document).ready(() => {
    let c = document.getElementById("myCanvas");
    let ctx = c.getContext("2d");
    let r = new Rectangulo(new Punto(3, 3), new Punto(100, 200), ctx);
    r.draw();

    c.onmousedown  = (evt) => {
        let mousePoint = new Punto(evt.clientX, evt.clientY);
        if(r.containsPoint(mousePoint)) {
            r.isDragged = true;
        }
    }

    c.onmouseup    = (evt) => {        
        if (r.isDragged) {
            delete r.isDragged;
        }
    }

    c.onmousemove  = (evt) => {
        if (r.isDragged) {
            let dx = evt.clientX - r.center.x;
            let dy = evt.clientY - r.center.y;            
            ctx.clearRect(0, 0, ctx.canvas.clientWidth, ctx.canvas.clientHeight);
            r.moveBy(dx,dy);
            r.draw();
        }
    }    
});


