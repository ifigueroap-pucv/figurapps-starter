let gulp = require('gulp');

gulp.task('build-js', () => {
    gulp.src('src/assets/js/**/*.js')
        .pipe(gulp.dest('dist/assets/js'))
})

gulp.task('build-images', () => {
    gulp.src('src/assets/images/**/*')
        .pipe(gulp.dest('dist/assets/images'))
})

gulp.task('build-css', () => {
    gulp.src('src/assets/styles/**/*')
        .pipe(gulp.dest('dist/assets/styles'))
})

gulp.task('watch', () => {
    gulp.watch('src/assets/js/**/*', ['build-js']);
    gulp.watch('src/assets/images/**/*', ['build-images']);
    gulp.watch('src/assets/styles/**/*', ['build-styles']);
})

gulp.task('build', ['build-js', 'build-images', 'build-css'])