export class Punto {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }

    moveBy(dx, dy) {
        return new Punto(this.x + dx, this.y + dy);
    }

    toString() {
        return `(${this.x}, ${this.y})`;
    }
}