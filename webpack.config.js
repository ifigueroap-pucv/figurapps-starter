const path = require('path');
let HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: "./src/main.js",

    output: {
        path: path.resolve(__dirname, "dist"),
        filename: "main_bundle.js",         
    },

    plugins: [
        new HtmlWebpackPlugin({
            template: './src/index.html'
        })
    
    ]
}
