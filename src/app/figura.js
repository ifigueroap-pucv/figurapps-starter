export class Figura {
    
    constructor(center) {        
        this.center = center;
    }

    moveBy(dx, dy) {
        this.center = new Point(this.center + dx, this.center.y + dy);
    }

    draw() {
        throw "Method Not Implemented!";
    }

    containsPoint(p) {
        throw "Method Not Implemented!";
    }
}
