import { Figura } from './figura';
import { Punto } from './punto';

export class Rectangulo extends Figura {

    constructor(topLeft, botRight, ctx) {
        let _x_center = (topLeft.x + botRight.x) / 2;
        let _y_center = (topLeft.y + botRight.y) / 2;        
        let _center   = new Punto(_x_center, _y_center);
        let _height = botRight.y - topLeft.y;
        let _width  = botRight.x - topLeft.x;

        super(_center);
        this.ctx = ctx;
        this.topLeft = topLeft;
        this.botRight = botRight;
        this.height = _height;
        this.width  = _width;
    }

    moveBy(dx, dy) {
        this.center   = this.center.moveBy(dx, dy);
        this.topLeft  = this.topLeft.moveBy(dx, dy);
        this.botRight = this.botRight.moveBy(dx, dy);
        this.height   = this.botRight.y - this.topLeft.y;
        this.width    = this.botRight.x - this.topLeft.x;
    }

    draw() {
        this.ctx.fillRect(this.topLeft.x, this.topLeft.y, this.width, this.height);        
    }

    containsPoint(p) {
        let xBound = p.x >= this.topLeft.x && p.x <= this.botRight.x;
        let yBound = p.y >= this.topLeft.y && p.y <= this.botRight.y;        
        return xBound && yBound;
    }

    toString() {
        return `Rectangle(${this.topLeft}, ${this.botRight}, ${this.width}, ${this.height})`;
    }    
}